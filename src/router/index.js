import Vue from 'vue'
import VueRouter from 'vue-router'
import About from '@/views/About.vue'
import Search from '@/views/Search.vue'

Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    redirect: '/search'
  },
  {
    path: '/search',
    name: 'Search',
    component: Search,
    meta: {
      title: 'Address Search',
      img: 'mdi-magnify'
    }
  },
  {
    path: '/about',
    name: 'About',    
    component: About,
    meta: {
      title: 'About',
      img: 'mdi-information'
    }
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
